#include <ros/ros.h>
#include <autoware_msgs/LaneArray.h>

class GlobalWaypointsSelector
{
private:
	ros::NodeHandle nh_, p_nh_;

	std::vector<ros::Subscriber> sub_global_waypoints_list_;
	bool init_flag_;

	void callbackWaypoints(const autoware_msgs::LaneArray &msg)
	{

	}
public:
	GlobalWaypointsSelector(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
		, init_flag_(false)
	{
		for(int cou=0; cou<10; cou++)
		{
			std::stringstream waypoints_name;
			waypoints_name << "waypoints_name" << cou+1;
			std::string get_waypoints_name = p_nh_.param<std::string>(waypoints_name.str(), "");
			if(get_waypoints_name == "")
			{
				std::cout << "error! : not param name [" << waypoints_name.str() << "]" << std::endl;
				return;
			}
        	ros::Subscriber sub = nh_.subscribe(get_waypoints_name, 1, &GlobalWaypointsSelector::callbackWaypoints, this);
			sub_global_waypoints_list_.push_back(sub);
		}

		init_flag_ = true;
	}

	bool is_topic_ok() {return init_flag_;}
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "global_waypoints_selector");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	GlobalWaypointsSelector gws(nh, private_nh);
	ros::spin();
	return 0;
}