#include <ros/ros.h>
//クロソイド曲線式のΦの計算関数
//phi0 : 初期角度
//phiV : もし曲率が変化しない場合にh分進んだ場合の角度増分(例えばφuが0なら、この曲線は円を描きます)
//phiU : クロソイドによる角度の増分
//S    : 0~1に正規化した曲線長上の位置
float phi(float phi0, float phiV, float phiU, float S) {
	return phi0 + phiV * S + phiU * S * S;
}

//クロソイド曲線の積分内部の計算
std::complex<float> slope_f(float phi0, float phiV, float phiU, float S) {
	std::complex<float> j(0.0f, 1.0f);
	return std::exp(j * phi(phi0, phiV, phiU, S));
}

//シンプソン則での数値積分
template <class T, class Real, class R>
void simpson_integral(T f, Real a, Real b, R *r) {
    Real mul = (b - a) * static_cast<Real>(1.0 / 6.0);
    *r = mul * (f(a) + static_cast<Real>(4.0) * f((a + b) * static_cast<Real>(0.5)) + f(b));
}

//積分内部用オブジェクト
struct Slope {
	float phi0;
	float phiV;
	float phiU;

	std::complex<float> operator()(float S) {
		return slope_f(phi0, phiV, phiU, S);
	}
};

struct XY
{
	double x,y;
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "clothoid_curve");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	int sgn = -1;
	double PX0 = 100.0;//x初期位置
	double PY1 = 1.5;//y終端位置
	double phi0 = 0 * M_PI / 180;//初期角度
	double phiV = sgn * 30 * M_PI / 180;//もし曲率が変化しない場合にh分進んだ場合の角度増分(例えばφuが0なら、この曲線は円を描きます)
	double phiU = sgn * 500 *M_PI / 180;//クロソイドによる角度の増分
	double h = 500.0f;//曲線の長さ
	int n = 5000;//ステップ数
	float stepS = 1.0 / n;

    Slope slope;
    slope.phi0 = phi0;
    slope.phiV = phiV;
    slope.phiU = phiU;

	std::complex<float> P_Vector;
	std::vector<XY> xy_list;
	XY center_xy;
	for(int i = 0 ; i < n ; ++i)
	{
		float S = stepS * i;

		std::complex<float> r;
		simpson_integral(slope, S, S + stepS, &r);
		P_Vector += r;

		float x = P_Vector.real();
		float y = P_Vector.imag();
		//double hx = h * x;
		//double hy = h * y;
		XY xy = {h*x, h*y}; 
		std::cout << "" << xy.x << "," << xy.y << std::endl;
		if((sgn == 1 && xy.y > PY1/2.0) || (sgn == -1 && xy.y < -PY1/2.0))
		{
			center_xy = xy;
			break;
		}
		xy_list.push_back(xy);
	}
	for(int i = xy_list.size()-1; i >= 0; i--)
	{
		XY xy = xy_list[i];
		double dtx = center_xy.x - xy.x;
		double dty = center_xy.y - xy.y;
		double x = center_xy.x + dtx;
		double y = center_xy.y + dty;
		std::cout << "" << x << "," << y << std::endl;
	}
	/*double phi0 = 0;//初期角度
	double phiV = 30 *M_PI / 180;//もし曲率が変化しない場合にh分進んだ場合の角度増分(例えばφuが0なら、この曲線は円を描きます)
	double phiU = 1000 *M_PI / 180;//クロソイドによる角度の増分
	double h = 1.0f;//曲線の長さ
	int n = 1000;//ステップ数
	float stepS = 1.0 / n;

    Slope slope;
    slope.phi0 = phi0;
    slope.phiV = phiV;
    slope.phiU = phiU;

	std::complex<float> P_Vector;
	for(int i = 0 ; i < n ; ++i) {
		float S = stepS * i;

		std::complex<float> r;
		simpson_integral(slope, S, S + stepS, &r);
		P_Vector += r;

		float x = P_Vector.real();
		float y = P_Vector.imag();
		double hx = h * x;
		double hy = h * y;
		std::cout << "" << hx << "," << hy << std::endl;
	}*/

	return 0;
}