#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <autoware_config_msgs/ConfigObstacleAvoidance.h>
#include <autoware_msgs/Lane.h>
#include <autoware_msgs/AvoidanceLaneInfoList.h>
#include <tf/transform_broadcaster.h>

double euclidDistance(geometry_msgs::Point p1, geometry_msgs::Point p2)
{
	double x = p1.x - p2.x;
	double y = p1.y - p2.y;
	double z = p1.z - p2.z;
	return sqrt(x*x + y*y + z*z);
}

void tf_quat_to_rpy(double& roll, double& pitch, double& yaw, tf::Quaternion quat){
	tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);  //rpy are Pass by Reference
}
void geometry_quat_to_rpy(double& roll, double& pitch, double& yaw, geometry_msgs::Quaternion geometry_quat){
	tf::Quaternion quat;
	quaternionMsgToTF(geometry_quat, quat);
	tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);  //rpy are Pass by Reference
}

//クロソイド曲線式のΦの計算関数
//phi0 : 初期角度
//phiV : もし曲率が変化しない場合にh分進んだ場合の角度増分(例えばφuが0なら、この曲線は円を描きます)
//phiU : クロソイドによる角度の増分
//S    : 0~1に正規化した曲線長上の位置
float phi(float phi0, float phiV, float phiU, float S) {
	return phi0 + phiV * S + phiU * S * S;
}

//クロソイド曲線の積分内部の計算
std::complex<float> slope_f(float phi0, float phiV, float phiU, float S) {
	std::complex<float> j(0.0f, 1.0f);
	return std::exp(j * phi(phi0, phiV, phiU, S));
}

//シンプソン則での数値積分
template <class T, class Real, class R>
void simpson_integral(T f, Real a, Real b, R *r) {
	Real mul = (b - a) * static_cast<Real>(1.0 / 6.0);
	*r = mul * (f(a) + static_cast<Real>(4.0) * f((a + b) * static_cast<Real>(0.5)) + f(b));
}

//積分内部用オブジェクト
struct Slope {
	float phi0;
	float phiV;
	float phiU;

	std::complex<float> operator()(float S) {
		return slope_f(phi0, phiV, phiU, S);
	}
};

//xy座標クラス
/*struct XY
{
	double x,y;
	double ratio;//変換する経路長を1とした場合の割合
};*/

//clothoidに変換するレーン情報
struct ConversionXY
{
	int waypoint_index_;//変換するwayointのインデックス
	//int waypoint_id_;//waypoint_paramのid
	autoware_msgs::AvoidanceLaneInfo xy_;//waypointのxy座標
};

//clothoid長と経路長の対応表用構造体
struct TableClothoidAndLane
{
	double clothoid_length_;
	double lane_length_;
};

//clothoid曲線を用いたS字回避経路の作成
//config : 各種clothoid param
//clothoid_length : clothoid曲線長
//rotation_center : ここを原点として作成したclothoidは回転する
//aviodance_flag : 回避経路作成だとtrue  帰り経路作成だとfalse
std::vector<autoware_msgs::AvoidanceLaneInfo> clothoid(const autoware_config_msgs::ConfigObstacleAvoidance config, const double clothoid_length, const bool avoidance_flag)
{
	int sgn = (avoidance_flag == true) ? -1 : 1;

	Slope slope;
	slope.phi0 = 0 * M_PI / 180.0;
	slope.phiU = sgn * config.phiU * M_PI / 180.0;
	slope.phiV = sgn * config.phiV * M_PI / 180.0;
	//std::cout << slope.phiU << "," << slope.phiV << std::endl;

	float stepS = 1.0 / config.step_count;
	std::complex<float> P_Vector;
	std::vector<autoware_msgs::AvoidanceLaneInfo> xy_list;
	autoware_msgs::AvoidanceLaneInfo center_xy;

	int counter = 0;
	for(int i = 0 ; i < config.step_count ; i++)
	{
		float S = stepS * i;

		std::complex<float> r;
		simpson_integral(slope, S, S + stepS, &r);
		P_Vector += r;

		float x = P_Vector.real();
		float y = P_Vector.imag();
		autoware_msgs::AvoidanceLaneInfo xy;// = {clothoid_length * x, clothoid_length * y, counter+1}; 
		xy.x = clothoid_length * x;  xy.y = clothoid_length * y;  xy.ratio = counter + 1;
		counter++;
		//std::cout << "" << xy.x << "," << xy.y << std::endl;
		if((sgn == 1 && xy.y > config.move_lane_length / 2.0) || (sgn == (-1) &&  xy.y < -config.move_lane_length / 2.0))
		{
			center_xy = xy;
			break;
		}
		xy_list.push_back(xy);
	}

	std::size_t list_size = xy_list.size();
	for(int i = list_size-1; i >= 0; i--)
	{
		autoware_msgs::AvoidanceLaneInfo xy = xy_list[i];
		double dtx = center_xy.x - xy.x;
		double dty = center_xy.y - xy.y;
		autoware_msgs::AvoidanceLaneInfo xyplus;// = {center_xy.x + dtx, center_xy.y + dty, counter+1};
		xyplus.x = center_xy.x + dtx;  xyplus.y = center_xy.y + dty;  xyplus.ratio = counter + 1;
		counter++;

		xy_list.push_back(xyplus);
		//std::cout << "" << xyplus.x << "," << xyplus.y << std::endl;
	}

	std::vector<autoware_msgs::AvoidanceLaneInfo> ret;
	for(autoware_msgs::AvoidanceLaneInfo xy : xy_list)
	{
		xy.ratio /= counter;
		ret.push_back(xy);
	}
	return ret;
}


//clothoid曲線を用いたS字回避経路の作成　出力確認用
//config : 各種clothoid param
//clothoid_length : clothoid曲線長
//aviodance_flag : 回避経路作成だとtrue  帰り経路作成だとfalse
std::vector<autoware_msgs::AvoidanceLaneInfo> clothoid_output(const autoware_config_msgs::ConfigObstacleAvoidance config, const double clothoid_length, const bool avoidance_flag)
{
	int sgn = (avoidance_flag == true) ? -1 : 1;

	Slope slope;
	slope.phi0 = 0 * M_PI / 180.0;
	slope.phiU = sgn * config.phiU * M_PI / 180.0;
	slope.phiV = sgn * config.phiV * M_PI / 180.0;
	//std::cout << slope.phiU << "," << slope.phiV << std::endl;

	float stepS = 1.0 / config.step_count;
	std::complex<float> P_Vector;
	std::vector<autoware_msgs::AvoidanceLaneInfo> xy_list;
	autoware_msgs::AvoidanceLaneInfo center_xy;

	int counter = 0;
	for(int i = 0 ; i < config.step_count ; i++)
	{
		float S = stepS * i;

		std::complex<float> r;
		simpson_integral(slope, S, S + stepS, &r);
		P_Vector += r;

		float x = P_Vector.real();
		float y = P_Vector.imag();
		autoware_msgs::AvoidanceLaneInfo xy;// = {clothoid_length * x, clothoid_length * y, counter+1}; 
		xy.x = clothoid_length * x;  xy.y = clothoid_length * y;  xy.ratio = counter + 1;
		counter++;
		//std::cout << "" << xy.x << "," << xy.y << std::endl;
		if((sgn == 1 && xy.y > config.move_lane_length / 2.0) || (sgn == (-1) &&  xy.y < -config.move_lane_length / 2.0))
		{
			center_xy = xy;
			break;
		}
		xy_list.push_back(xy);
	}

	std::size_t list_size = xy_list.size();
	for(int i = list_size-1; i >= 0; i--)
	{
		autoware_msgs::AvoidanceLaneInfo xy = xy_list[i];
		double dtx = center_xy.x - xy.x;
		double dty = center_xy.y - xy.y;
		autoware_msgs::AvoidanceLaneInfo xyplus;// = {center_xy.x + dtx, center_xy.y + dty, counter+1};
		xyplus.x = center_xy.x + dtx;  xyplus.y = center_xy.y + dty;  xyplus.ratio = counter + 1;
		counter++;

		xy_list.push_back(xyplus);
		//std::cout << "" << xyplus.x << "," << xyplus.y << std::endl;
	}

	std::vector<autoware_msgs::AvoidanceLaneInfo> ret;
	for(autoware_msgs::AvoidanceLaneInfo xy : xy_list)
	{
		xy.ratio /= counter;
		ret.push_back(xy);
	}
	return ret;
}

//S字クロソイド曲線を経路の位置に移動させる
//clothoid_list : S字クロソイド
//move_center : 移動させる位置と回転
std::vector<autoware_msgs::AvoidanceLaneInfo> clothoid_move(const std::vector<autoware_msgs::AvoidanceLaneInfo> clothoid_list, const geometry_msgs::Pose move_center)
{
	tf::Quaternion qua_center;
	qua_center.setX(move_center.orientation.x); qua_center.setY(move_center.orientation.y);
	qua_center.setZ(move_center.orientation.z); qua_center.setW(move_center.orientation.w);
	double yaw = tf::getYaw(qua_center);
	tf::Quaternion qua_yaw = tf::createQuaternionFromYaw(-yaw);
	//qua_yaw.setRPY(0,0,0*M_PI/180.0);

	std::vector<autoware_msgs::AvoidanceLaneInfo> ret;
	for(int i = 0; i < clothoid_list.size(); i++)
	{
		autoware_msgs::AvoidanceLaneInfo xy = clothoid_list[i];
		tf::Quaternion p;
		p.setX(xy.x); p.setY(xy.y); p.setZ(0); p.setW(0);
		tf::Quaternion rot_qua = qua_yaw.inverse() * p * qua_yaw;

		autoware_msgs::AvoidanceLaneInfo xy_ret;
		xy_ret.x = rot_qua.getX() + move_center.position.x;
		xy_ret.y = rot_qua.getY() + move_center.position.y;
		xy_ret.ratio = xy.ratio;
		//std::cout << xy_ret.x_ << "," << xy_ret.y_ << std::endl;
		ret.push_back(xy_ret);
	}

	return ret;
}

geometry_msgs::Pose clothoidOrigin(const std::vector<ConversionXY> list, geometry_msgs::Point position)
{
	ConversionXY cxy1 = list[0],  cxy2 = list[list.size()-1];
	double yaw = atan2(cxy2.xy_.y - cxy1.xy_.y, cxy2.xy_.x - cxy1.xy_.x); std::cout << yaw << std::endl;
	geometry_msgs::Pose ret;
	ret.position = position;
	tf::quaternionTFToMsg(tf::createQuaternionFromYaw(yaw), ret.orientation);
	return ret;
}

class ObstacleAvoidance
{
private:
	static const int AVOIDANCE_SWITCH_OFF = 0;//障害物回避なし
	static const int AVOIDANCE_SWITCH_GO_CREATING = 1;//障害物回避作成待ち
	static const int AVOIDANCE_SWITCH_GO_ON = 2;//障害物回避実行中
	static const int AVOIDANCE_SWITCH_RETURN_CREATE = 3;//戻り経路作成待ち
	static const int AVOIDANCE_SWITCH_RETURN_ON = 4;//戻り経路実行中

	ros::NodeHandle nh_, p_nh_;

	ros::Publisher pub_avoidance_lane_info_list_;
	ros::Subscriber sub_config_, sub_local_waypoints_, sub_avoidance_switch_;

	autoware_config_msgs::ConfigObstacleAvoidance config_;
	int avoidance_switch_;//障害物回避フラグ 詳細は上記のAVOIDANCE_SWITCH定数を参照
	std::vector<TableClothoidAndLane> table_clothoid_and_lane_;//clothoid長とレーン長の対応表
	autoware_msgs::Lane local_waypoints_;
	autoware_msgs::AvoidanceLaneInfoList avoidance_list_;

	void callbackConfig(const autoware_config_msgs::ConfigObstacleAvoidance &msg)
	{
		config_ = msg;
		createCorrespondenceTable(true);
	}

	void callbackLocalWaypoints(const autoware_msgs::Lane &msg)
	{
		local_waypoints_ = msg;
	}

	//回避行動を行うかのフラグのcallback
	void callbackAvoidanceSwitch(const std_msgs::Bool &msg)
	{
		if(msg.data == true)//回避経路作成
		{
			if(avoidance_switch_ == AVOIDANCE_SWITCH_OFF)
				avoidance_switch_ = ObstacleAvoidance::AVOIDANCE_SWITCH_GO_CREATING;
		}
		else
		{
			if(avoidance_switch_ == AVOIDANCE_SWITCH_GO_ON)//戻り経路作成
			{
				avoidance_switch_ == ObstacleAvoidance::AVOIDANCE_SWITCH_RETURN_CREATE;
			}
		}
	}

	//S字経路に変換するwaypointの取得
	std::vector<ConversionXY> getMoveWaypoints(const int start_index, const double max_length)
	{
		std::vector<ConversionXY> ret;
		double dt_sum = 0;
		for(int ind=start_index+1, prev_ind=start_index; ind<local_waypoints_.waypoints.size(); ind++, prev_ind++)
		{
			const geometry_msgs::Point &wp_new = local_waypoints_.waypoints[ind].pose.pose.position;
			const geometry_msgs::Point &wp_prev = local_waypoints_.waypoints[prev_ind].pose.pose.position;
			double dt = euclidDistance(wp_new, wp_prev);
			dt_sum += dt;
			if(dt_sum >= max_length)
				break;
			else
			{
				ConversionXY cxy;
				cxy.waypoint_index_ = ind;
				cxy.xy_.waypoint_id = local_waypoints_.waypoints[ind].waypoint_param.id;
				cxy.xy_.x = wp_new.x;
				cxy.xy_.y = wp_new.y;
				cxy.xy_.ratio = dt_sum / max_length;
				ret.push_back(cxy);

				//std::cout << std::fixed;
				//std::cout << std::setprecision(5) << cxy.waypoint_index_ << "," << cxy.waypoint_id_ << "," << cxy.xy_.x << "," << cxy.xy_.y << "," << cxy.ratio_ << std::endl;
				//std::cout << std::setprecision(5) << cxy.waypoint_index_ << "," << cxy.waypoint_id_ << "," << dt_sum << std::endl;
			}
		}

		return ret;
	}

	//clothoid曲線長と経路長との対応表作成
	//const double table_clothoid_length = 120.0;//clothoid曲線長の最大値
	//対応表はable_clothoid_and_lane_メンバ変数に入る
	//aviodance_flag : 回避経路作成だとtrue  帰り経路作成だとfalse
	const double table_step_ = 0.5;//曲線長ステップ値
	const double table_lane_length_ = 120.0;//レーン長の最大値
	void createCorrespondenceTable(bool avoidance_flag)
	{
		table_clothoid_and_lane_.clear();
		//for(double h=table_step; h<=table_clothoid_length; h+=table_step)
		for(double h=table_step_; true; h+=table_step_)
		{
			//double clothoid_length = h * h / 8.0;
			std::vector<autoware_msgs::AvoidanceLaneInfo> xy_list = clothoid(config_, h, avoidance_flag);//clothoid_length);
			TableClothoidAndLane t;
			t.clothoid_length_ = h;
			t.lane_length_ = xy_list[xy_list.size()-1].x;//xy_list.end()->x;;
			table_clothoid_and_lane_.push_back(t);
			//std::fixed;
			//std::cout << std::setprecision(10) << t.clothoid_length_ << "," << t.lane_length_ << std::endl;
			if(t.lane_length_ > table_lane_length_) break;
		}
	}

	//空の回避経路情報をpublish（回避しない）
	//stamp : タイムスタンプ
	//frame_id : 所属tfフレームの名前
	void emptyWaypointsInfoPublish()
	{
		autoware_msgs::AvoidanceLaneInfoList list;
		list.header.stamp = ros::Time(0);
		list.header.frame_id = "";
		pub_avoidance_lane_info_list_.publish(list);
	}

	//作成した回避経路情報をpublish
	void avoidWaypointsInfoPublish(ros::Time stamp)
	{
		ros::Time time = avoidance_list_.header.stamp;
		avoidance_list_.header.stamp = stamp;
		pub_avoidance_lane_info_list_.publish(avoidance_list_);
		avoidance_list_.header.stamp = time;
	}

	//回避経路のS字箇所の情報をpublish
	//conversion_lane : S字変換する経路情報
	//clothoid_data : clothoid曲線から作成したS字情報
	//stamp : タイムスタンプ
	//frame_id : 所属tfフレームの名前
	void createAvoidWaypointsInfoPublish(const std::vector<ConversionXY> conversion_lane, const std::vector<autoware_msgs::AvoidanceLaneInfo> clothoid_data)
	{
		avoidance_list_.list.clear();
		int start_index = conversion_lane[0].waypoint_index_;

		int wayi;
		for(wayi=0; wayi<conversion_lane.size(); wayi++)
		{
			int cloi = 0;
			while(cloi < clothoid_data.size())
			{
				ConversionXY lane_xy = conversion_lane[wayi];
				autoware_msgs::AvoidanceLaneInfo clo_xy = clothoid_data[cloi];
				if(lane_xy.xy_.ratio < clo_xy.ratio)
				{
					clo_xy.waypoint_id = lane_xy.xy_.waypoint_id;
					avoidance_list_.list.push_back(clo_xy);
					break;
				}
				cloi++;
			}
		}

		avoidance_list_.header.stamp = local_waypoints_.header.stamp;
		avoidance_list_.header.frame_id = local_waypoints_.header.frame_id;
		pub_avoidance_lane_info_list_.publish(avoidance_list_);
	}
public:
	ObstacleAvoidance(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
		, avoidance_switch_(false)
	{
		pub_avoidance_lane_info_list_ = nh_.advertise<autoware_msgs::AvoidanceLaneInfoList>("/avoidance_lane_info_list", 1);
		sub_config_ = nh_.subscribe("/config/obstacle_avoidance", 1, &ObstacleAvoidance::callbackConfig, this);
		sub_local_waypoints_ = nh_.subscribe("/safety_waypoints", 1, &ObstacleAvoidance::callbackLocalWaypoints, this);
		sub_avoidance_switch_ = nh_.subscribe("/avoidance_switch", 1, &ObstacleAvoidance::callbackAvoidanceSwitch, this);

		//clothoid 初期パラメータ代入
		config_.move_lane_length = 1.5;////y終端位置（経路のy方向の終わり）
		config_.phiV = 30;//もし曲率が変化しない場合にh分進んだ場合のdeg角度増分(例えばφuが0なら、この曲線は円を描きます)
		config_.phiU = 500;//クロソイドによるdeg角度の増分
		config_.step_count = 5000;//計算ステップ数（あまり小さいと分解能が下がる）

		createCorrespondenceTable(true);
	}

	void run()
	{
		ros::Time nowtime = ros::Time::now();

		if(local_waypoints_.waypoints.size() == 0)
		{
			emptyWaypointsInfoPublish();
		}

		switch(avoidance_switch_)
		{
			case ObstacleAvoidance::AVOIDANCE_SWITCH_OFF:
			{
				emptyWaypointsInfoPublish();
				break;
			}
			case ObstacleAvoidance::AVOIDANCE_SWITCH_GO_CREATING:
			{
				if(local_waypoints_.waypoints.size() < 2)
				{
					emptyWaypointsInfoPublish();
					break;
				}

				//クロソイド曲線で移動させる経路の長さpath_length文のクロソイドを計算
				double path_length = 50.0;//clothoidを適用する経路の長さ　現在は暫定的に固定値
				int start_index = 0;//回避経路を作成するwaypointのインデックス(1ならbase_linkの次)　現在は暫定的にbase_linkの次のwaypoint

				//S字変換するwaypoints情報の取得
				std::vector<ConversionXY> conversion_lane_xy = getMoveWaypoints(start_index, path_length);

				//対応表から使用するclothoid長を検索
				double clothoid_length = table_clothoid_and_lane_[table_clothoid_and_lane_.size()-1].clothoid_length_;
				for(int i=0;i<table_clothoid_and_lane_.size() ;i++)
				{
					if(table_clothoid_and_lane_[i].lane_length_ > path_length)
					{
						clothoid_length = table_clothoid_and_lane_[i].clothoid_length_;
						break;
					}
				}

				//double roll, pitch, yaw;//S字経路を回転させるために経路からyaw角を取得
				//geometry_quat_to_rpy(roll, pitch, yaw, msg.waypoints[start_index].pose.pose.orientation);

				//x方向を横軸とした、高さがconfig_.move_lane_lengthまでのS字クロソイドを作成
				std::vector<autoware_msgs::AvoidanceLaneInfo> xy_list = clothoid(config_, clothoid_length, true);
				//
				geometry_msgs::Pose origin = clothoidOrigin(conversion_lane_xy, local_waypoints_.waypoints[start_index].pose.pose.position);
				//作成したclothoid曲線を経路の位置に移動
				std::vector<autoware_msgs::AvoidanceLaneInfo> xy_rot_list = clothoid_move(xy_list, origin);//local_waypoints_.waypoints[start_index].pose.pose);
				createAvoidWaypointsInfoPublish(conversion_lane_xy, xy_rot_list);

				avoidance_switch_ = ObstacleAvoidance::AVOIDANCE_SWITCH_GO_ON;
				break;
			}
			case ObstacleAvoidance::AVOIDANCE_SWITCH_GO_ON:
			{
				avoidWaypointsInfoPublish(nowtime);
				break;
			}
			case ObstacleAvoidance::AVOIDANCE_SWITCH_RETURN_CREATE:
			{
				emptyWaypointsInfoPublish();
				break;
			}
			case ObstacleAvoidance::AVOIDANCE_SWITCH_RETURN_ON:
			{
				emptyWaypointsInfoPublish();
				break;
			}
			default:
			{
				emptyWaypointsInfoPublish();
			}
		}
	}
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "obstacle_avoidance");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	ObstacleAvoidance oa(nh, private_nh);
	ros::Rate rate(30);
	while(ros::ok())
	{
		ros::spinOnce();
		oa.run();
		rate.sleep();
	}
	return 0;
}