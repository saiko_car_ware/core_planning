#include <ros/ros.h>
#include <autoware_msgs/Lane.h>
#include <std_msgs/String.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <autoware_msgs/WaypointPositionAdjustmentOut.h>

class LocalWaypointAdjustment
{
private:
	ros::NodeHandle nh_, p_nh_;

	ros::Publisher pub_out_, pub_outtext_;
	ros::Subscriber sub_local_waypoints_;

	tf::TransformListener tf_listener_;
	tf::TransformBroadcaster tf_broadcaster_;

	void point_adjustment(double lineX1, double lineY1, double lineX2, double lineY2,
						  double pointX, double pointY, double *retX, double *retY)
	{
		double x1 = lineX1, x2 = lineX2;
		double y1 = lineY1, y2 = lineY2;
		double a = y2 - y1;
		double b = x1 - x2;
		double c = - x1 * y2 + y1 * x2;
		double x0 = pointX, y0 = pointY;
		double x_numerator = b * (b * x0 - a * y0) - a * c;
		double x_denominator = a * a + b * b;
		*retX = x_numerator / x_denominator;
		double y_numerator = a * (-b * x0 + a * y0) - b * c;
		double y_denominator = a * a + b * b;
		*retY = y_numerator / y_denominator;
	}

	double distance(geometry_msgs::Point p1, geometry_msgs::Point p2)
	{
		double x = p1.x - p2.x;
		double y = p1.y - p2.y;
		double z = p1.z - p2.z;
		return sqrt(x*x + y*y + z*z);
	}

	void callbackLocalWaypoints(const autoware_msgs::Lane &msg)
	{
		tf::StampedTransform tf_front_baselink;
		ros::Time nowtime = ros::Time::now();
		bool transform_flag = tf_listener_.waitForTransform("/map", "/front_base_link", nowtime, ros::Duration(3.0));
		if(!transform_flag) {std::cerr << "error waitForTransform" << std::endl;  return;}
		try{ tf_listener_.lookupTransform("/map", "/front_base_link", nowtime, tf_front_baselink); }
		catch(const tf::TransformException& e) {std::cerr << "error waitForTransform" << std::endl;  return;}

		double min_dis = 10000000000;
		int min_param_id = 0, min_way_id = 0;
		for(int id=0; id<msg.waypoints.size(); id++)
		{
			double x = msg.waypoints[id].pose.pose.position.x - tf_front_baselink.getOrigin().getX();
			double y = msg.waypoints[id].pose.pose.position.y - tf_front_baselink.getOrigin().getY();
			double z = msg.waypoints[id].pose.pose.position.z - tf_front_baselink.getOrigin().getZ();
			double dis = sqrt(x*x + y*y + z*z); //std::cout << dis << std::endl;
			if(dis < min_dis)
			{
				min_dis = dis;
				min_param_id = msg.waypoints[id].waypoint_param.id;//std::cout << min_id << std::endl;
				min_way_id = id;
			}
		}

		double dis_front = distance(msg.waypoints[min_way_id].pose.pose.position, msg.waypoints[min_way_id-1].pose.pose.position);
		double dis_next = distance(msg.waypoints[min_way_id].pose.pose.position, msg.waypoints[min_way_id+1].pose.pose.position);
		int pair_id = (dis_front < dis_next) ? min_way_id-1 : min_way_id+1;
		double adj_x, adj_y;
		point_adjustment(msg.waypoints[min_way_id].pose.pose.position.x, msg.waypoints[min_way_id].pose.pose.position.y,
						 msg.waypoints[pair_id].pose.pose.position.x, msg.waypoints[pair_id].pose.pose.position.y,
						 tf_front_baselink.getOrigin().getX(), tf_front_baselink.getOrigin().getY(), &adj_x, &adj_y);

		/*std::cout << std::fixed;
		std::cout << std::setprecision(10) << "map to forntbase point : " << tf_front_baselink.getOrigin().getX() << "," << tf_front_baselink.getOrigin().getY() << "," << tf_front_baselink.getOrigin().getZ() << std::endl;
		std::cout << std::setprecision(10) << "nearby waypoints : " << msg.waypoints[min_id].pose.pose.position.x << "," << msg.waypoints[min_id].pose.pose.position.y << "," << msg.waypoints[min_id].pose.pose.position.z << std::endl;
		double diff_x = tf_front_baselink.getOrigin().getX() - msg.waypoints[min_id].pose.pose.position.x;
		double diff_y = tf_front_baselink.getOrigin().getY() - msg.waypoints[min_id].pose.pose.position.y;
		double diff_z = tf_front_baselink.getOrigin().getZ() - msg.waypoints[min_id].pose.pose.position.z;
		std::cout << std::setprecision(10) << "difference : " << diff_x << "," << diff_y << "," << diff_z << std::endl;*/

		std::stringstream str;
		str << std::fixed;
		str << std::setprecision(10) << "map to forntbase point : " << tf_front_baselink.getOrigin().getX() << "," << tf_front_baselink.getOrigin().getY() << "," << tf_front_baselink.getOrigin().getZ() << "\n";
		str << std::setprecision(10) << "neary waypoints : " << min_param_id << "," << msg.waypoints[min_way_id].pose.pose.position.x << "," << msg.waypoints[min_way_id].pose.pose.position.y << "," << msg.waypoints[min_way_id].pose.pose.position.z << "\n";
		str << std::setprecision(10) << "adjjustment : " << adj_x << "," << adj_y << "," << msg.waypoints[min_way_id].pose.pose.position.z << "\n";
		double diff_x = adj_x - msg.waypoints[min_way_id].pose.pose.position.x;
		double diff_y = adj_y - msg.waypoints[min_way_id].pose.pose.position.y;
		double diff_z = tf_front_baselink.getOrigin().getZ() - msg.waypoints[min_way_id].pose.pose.position.z;
		str << std::setprecision(10) << "difference : " << diff_x << "," << diff_y << "," << diff_z << "\n";

		tf::Transform transform1, transform2;
		transform1.setOrigin( tf::Vector3(msg.waypoints[min_way_id].pose.pose.position.x,
		                                 msg.waypoints[min_way_id].pose.pose.position.y,
										 msg.waypoints[min_way_id].pose.pose.position.z) );
		transform1.setRotation( tf::Quaternion(msg.waypoints[min_way_id].pose.pose.orientation.x,
		                                      msg.waypoints[min_way_id].pose.pose.orientation.y,
											  msg.waypoints[min_way_id].pose.pose.orientation.z,
											  msg.waypoints[min_way_id].pose.pose.orientation.w));
		tf_broadcaster_.sendTransform(tf::StampedTransform(transform1, nowtime, "/map", "/frontbase_tmp1"));
		transform2.setOrigin( tf::Vector3(adj_x, adj_y, tf_front_baselink.getOrigin().getZ()));
		transform2.setRotation(transform1.getRotation());
		tf_broadcaster_.sendTransform(tf::StampedTransform(transform2, nowtime, "/map", "/frontbase_tmp2"));
		tf::StampedTransform get_tf_tmp;
		tf_listener_.waitForTransform("/frontbase_tmp1", "/frontbase_tmp2", nowtime, ros::Duration(3.0));
		tf_listener_.lookupTransform("/frontbase_tmp1", "/frontbase_tmp2", nowtime, get_tf_tmp);
		str << std::setprecision(10) << "wayline_diff : " << get_tf_tmp.getOrigin().getX() << "," << get_tf_tmp.getOrigin().getY() << "," << get_tf_tmp.getOrigin().getZ() << "\n";
		
		std_msgs::String str_ret;
		str_ret.data = str.str();
		pub_outtext_.publish(str_ret);

		autoware_msgs::WaypointPositionAdjustmentOut ret;
		ret.map_to_frontbase_point.x = tf_front_baselink.getOrigin().getX();
		ret.map_to_frontbase_point.y = tf_front_baselink.getOrigin().getY();
		ret.map_to_frontbase_point.z = tf_front_baselink.getOrigin().getZ();
		ret.neary_waypoint.x = msg.waypoints[min_way_id].pose.pose.position.x;
		ret.neary_waypoint.y = msg.waypoints[min_way_id].pose.pose.position.y;
		ret.neary_waypoint.z = msg.waypoints[min_way_id].pose.pose.position.z;
		ret.near_waypoint_id = min_param_id;
		ret.adjjustment_neary_waypoint.x = adj_x;
		ret.adjjustment_neary_waypoint.y = adj_y;
		ret.adjjustment_neary_waypoint.z = msg.waypoints[min_way_id].pose.pose.position.z;
		ret.difference.x = diff_x;
		ret.difference.y = diff_y;
		ret.difference.z = diff_z;
		ret.way_line_diff.x = get_tf_tmp.getOrigin().getX();
		ret.way_line_diff.y = get_tf_tmp.getOrigin().getY();
		ret.way_line_diff.z = get_tf_tmp.getOrigin().getZ();
		pub_out_.publish(ret);
	}
public:
	LocalWaypointAdjustment(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
	{
		pub_out_ = nh.advertise<autoware_msgs::WaypointPositionAdjustmentOut>("/waypoint_position_adjustment_frontbase/out", 10);
		pub_outtext_ = nh.advertise<std_msgs::String>("/waypoint_position_adjustment_frontbase/outtext", 10);
		sub_local_waypoints_ = nh_.subscribe("/final_waypoints", 10, &LocalWaypointAdjustment::callbackLocalWaypoints, this);
	}
};

int main(int argc, char** argv)
{
   	ros::init(argc, argv, "waypoint_position_adjustment_frontbase");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	LocalWaypointAdjustment lwa(nh, private_nh);
	ros::spin();
	return 0;
}
