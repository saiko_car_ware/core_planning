#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64.h>
#include <tf/transform_broadcaster.h>
#include <autoware_config_msgs/ConfigLaneAppropriateChecker.h>
#include <autoware_msgs/Lane.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>

double sign(double A){
    return (A>0)-(A<0);
}

double euclidDistance(geometry_msgs::Point p1, geometry_msgs::Point p2)
{
	double x = p1.x - p2.x;
	double y = p1.y - p2.y;
	double z = p1.z - p2.z;
	return sqrt(x*x + y*y + z*z);
}

double euclidDistanceXY(geometry_msgs::Point p1, geometry_msgs::Point p2)
{
	double x = p1.x - p2.x;
	double y = p1.y - p2.y;
	return sqrt(x*x + y*y);
}

void geometry_quat_to_rpy(double& roll, double& pitch, double& yaw, const geometry_msgs::Quaternion geometry_quat){
	tf::Quaternion quat;
	quaternionMsgToTF(geometry_quat, quat);
	tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);  //rpy are Pass by Reference
}

class LaneAppropriateChecker
{
private:
	ros::NodeHandle nh_, p_nh_;

	ros::Publisher pub_local_waypoints_, pub_tmp_;
	ros::Subscriber sub_config_, sub_local_waypoints_;

	autoware_config_msgs::ConfigLaneAppropriateChecker config_;

	double math_pitch(geometry_msgs::Point p1, geometry_msgs::Point p2)
	{
		double euc_xy_ = euclidDistanceXY(p1,p2);
		double diff_z = p2.z - p1.z;
		return atan2(diff_z, euc_xy_);
	}

	void callbackConfig(const autoware_config_msgs::ConfigLaneAppropriateChecker &msg)
	{
		config_ = msg;
	}

	//経路速度変更関数  元はlane_ruleの関数
	autoware_msgs::Lane apply_acceleration(const autoware_msgs::Lane& lane, double acceleration, int start_index,
										   size_t fixed_cnt, double fixed_vel)
	{
	  autoware_msgs::Lane l = lane;

	  if (fixed_cnt == 0)
		return l;

	  double square_vel = fixed_vel * fixed_vel;
	  double distance = 0;
	  for (int i = start_index; i < l.waypoints.size(); ++i)
	  {
		if (i - start_index < fixed_cnt)
		{
			l.waypoints[i].twist.twist.linear.x = fixed_vel;
			continue;
		}

		geometry_msgs::Point a = l.waypoints[i - 1].pose.pose.position;
		geometry_msgs::Point b = l.waypoints[i].pose.pose.position;
		distance += hypot(b.x - a.x, b.y - a.y);

		double v = sqrt(square_vel + 2 * acceleration * distance);
		if (v < l.waypoints[i].twist.twist.linear.x)
			l.waypoints[i].twist.twist.linear.x = v;
		else
			break;
	  }

	  return l;
	}

	//各waypointの曲がり具合を数値化
	void curveCheck(const autoware_msgs::Lane &lane)
	{
		if(lane.waypoints.size() == 0) return;

		autoware_msgs::Lane copy_lane = lane;//publish用
		//for(int i=0;i<copy_lane.waypoints.size();i++) copy_lane.waypoints[i].twist.twist.linear.x = 40/3.6;//検証用

		//double max_curve_rate_abs = 0;
		//double max_curve_ind = -1;
		std::vector<double> curve_rate_list_;
		double pitch_sum = 0;
		for(int basis_ind=0; basis_ind<copy_lane.waypoints.size(); basis_ind++)//カーブ情報を計算したいwaypointのインデックス
		{
			autoware_msgs::Waypoint &basis_way = copy_lane.waypoints[basis_ind];//現在のwaypoint

			//basis_indの位置と向きを取得
			geometry_msgs::Pose &basis_pose = basis_way.pose.pose;
			Eigen::Vector3d basis_po(basis_pose.position.x, basis_pose.position.y, basis_pose.position.z);//位置

			double roll, pitch, yaw;
			geometry_quat_to_rpy(roll, pitch, yaw, basis_pose.orientation);
			Eigen::Quaterniond basis_qua = Eigen::Quaterniond(Eigen::AngleAxisd(-yaw, Eigen::Vector3d::UnitZ()));//向き

			Eigen::Vector3d range_sum = Eigen::Vector3d(0.0, 0.0, 0.0);//周辺のwaypoint間距離の総和
			int count = 0;
			double pose_dt_sum = 0;
			for(int i=basis_ind+1, j=basis_ind; i<copy_lane.waypoints.size(); i++, j++, count++)//conig_.config_.check_waypoint_rangesの範囲にあるwaypointのインデックス(上)
			{
				geometry_msgs::Pose &pose = copy_lane.waypoints[i].pose.pose;
				geometry_msgs::Pose &pose_prev = copy_lane.waypoints[j].pose.pose;
				pose_dt_sum += euclidDistance(pose.position, pose_prev.position);
				if(pose_dt_sum > config_.check_waypoint_ranges) break;
				//basis_indの位置と向きを基準としたiの座標の差分をrange_sumにプラス
				geometry_msgs::Point &point = copy_lane.waypoints[i].pose.pose.position;
				Eigen::Vector3d po(point.x, point.y, point.z);
				po -= basis_po;
				Eigen::Vector3d po_rot = basis_qua * po;
				range_sum += po_rot;

				std::cout << po_rot.y() << std::endl;
			}
			pose_dt_sum = 0;
			for(int i=basis_ind-1, j=basis_ind; i>=0; i--, j--, count++)//conig_.config_.check_waypoint_rangesの範囲にあるwaypointのインデックス(下)
			{
				geometry_msgs::Pose &pose = copy_lane.waypoints[i].pose.pose;
				geometry_msgs::Pose &pose_prev = copy_lane.waypoints[j].pose.pose;
				pose_dt_sum += euclidDistance(pose.position, pose_prev.position);
				if(pose_dt_sum > config_.check_waypoint_ranges) break;
				//basis_indの位置と向きを基準としたiの座標の差分をrange_sumにプラス
				geometry_msgs::Point &point = copy_lane.waypoints[i].pose.pose.position;
				Eigen::Vector3d po(point.x, point.y, point.z);
				po -= basis_po;
				Eigen::Vector3d po_rot = basis_qua * po;
				range_sum += po_rot;
			}

			Eigen::Vector3d range_ave;
			if(count != 0)
			{
				//Eigen::Vector3d range_ave = range_sum / count; //できない？
				range_ave[0] = range_sum[0] / count;
				range_ave[1] = range_sum[1] / count;
				range_ave[2] = range_sum[2] / count;
			}
			else range_ave.Zero();
			double curve_rate = range_ave.y();
			copy_lane.waypoints[basis_ind].waypoint_param.curve_rate = curve_rate;
			/*if(max_curve_rate_abs < fabs(curve_rate))
			{
				max_curve_rate_abs = fabs(curve_rate);
				max_curve_ind = basis_ind;
			}*/
			curve_rate_list_.push_back(curve_rate);

			{
				char buf[100];
				//sprintf(buf, "%lf,%lf,%lf", range_ave.x(), range_ave.y(), range_ave.z());
				//printf("%d,%lf,%lf,%lf,%d\n", lane.waypoints[basis_ind].waypoint_param.id, range_ave.x(), range_ave.y(), range_ave.z(), count);
				sprintf(buf, ",%d,%lf,", basis_way.waypoint_param.id, curve_rate);
				std_msgs::String str;
				str.data = std::string(buf);
				pub_tmp_.publish(str);
			}

			//経路のピッチを計算
			const int pitch_range = 5;
			std::vector<double> pitch_list;
			for(int i=basis_ind; i<=basis_ind+pitch_range-1 && i<copy_lane.waypoints.size(); i++)
			{
				autoware_msgs::Waypoint &way1 = copy_lane.waypoints[i];
				autoware_msgs::Waypoint &way2 = copy_lane.waypoints[i+1];
				geometry_msgs::Point p1 = way1.pose.pose.position;
				geometry_msgs::Point p2 = way2.pose.pose.position;
				pitch_list.push_back(math_pitch(p1, p2));
			}
			for(int i=basis_ind; i<=basis_ind-pitch_range+1; i--)
			{
				autoware_msgs::Waypoint &way1 = copy_lane.waypoints[i-1];
				autoware_msgs::Waypoint &way2 = copy_lane.waypoints[i];
				geometry_msgs::Point p1 = way1.pose.pose.position;
				geometry_msgs::Point p2 = way2.pose.pose.position;
				pitch_list.push_back(math_pitch(p1, p2));
			}
			std::sort(pitch_list.begin(), pitch_list.end());
			double pitch_ave = pitch_list[pitch_list.size()/2];
			copy_lane.waypoints[basis_ind].waypoint_param.pitch = pitch_ave;
			pitch_sum += pitch_ave;
		}
		copy_lane.pitch_average = pitch_sum / copy_lane.waypoints.size();

		pub_local_waypoints_.publish(copy_lane);
		//極大チェック
		/*std::vector<int> maximal_ind_list;
		std::vector<double> maximal_rate_list;
		for(int basis_ind=0; basis_ind<curve_rate_list_.size(); basis_ind++)
		{
			//std::cout << i << "," << curve_rate_list_[i] << std::endl;
			double basis_rate = curve_rate_list_[basis_ind];

			if(fabs(basis_rate) > config_.curve_rate_min)
			{
				double prev_rate_dt = 0;
				bool maximal_flag = true;
				double pose_dt_sum = 0;
				for(int i=basis_ind+1, j=basis_ind; i<copy_lane.waypoints.size(); i++, j++)//conig_.config_.check_waypoint_rangesの範囲にあるwaypointのインデックス(上)
				{
					geometry_msgs::Pose &pose = copy_lane.waypoints[i].pose.pose;
					geometry_msgs::Pose &pose_prev = copy_lane.waypoints[j].pose.pose;
					pose_dt_sum += euclidDistance(pose.position, pose_prev.position);
					if(pose_dt_sum > config_.check_waypoint_ranges) break;

					double rate_dt = basis_rate - curve_rate_list_[i];//std::cout << basis_ind << "," << rate_dt << "," << prev_rate_dt << std::endl;
					if(prev_rate_dt != 0)
					{
						if(sign(rate_dt) != sign(prev_rate_dt)) maximal_flag = false;
					} 
					prev_rate_dt = rate_dt;
				}
				pose_dt_sum = 0;
				for(int i=basis_ind-1, j=basis_ind; i>=0; i--, j--)//conig_.config_.check_waypoint_rangesの範囲にあるwaypointのインデックス(下)
				{
					geometry_msgs::Pose &pose = copy_lane.waypoints[i].pose.pose;
					geometry_msgs::Pose &pose_prev = copy_lane.waypoints[j].pose.pose;
					pose_dt_sum += euclidDistance(pose.position, pose_prev.position);
					if(pose_dt_sum > config_.check_waypoint_ranges) break;

					double rate_dt = basis_rate - curve_rate_list_[i];//std::cout << basis_ind << "," << rate_dt << "," << prev_rate_dt << std::endl;
					if(prev_rate_dt != 0)
					{
						if(sign(rate_dt) != sign(prev_rate_dt)) maximal_flag = false;
					} 
					prev_rate_dt = rate_dt;
				}

				//int a = (maximal_flag == true) ? 1 : 0;
				//std::cout << copy_lane.waypoints[basis_ind].waypoint_param.id << "," << a << std::endl;
				if(maximal_flag == true)
				{
					maximal_ind_list.push_back(basis_ind);
					maximal_rate_list.push_back(basis_rate);
				}
			}
		}

		//if(max_curve_rate_abs > config_.curve_rate_min)
		autoware_msgs::Lane l = copy_lane;
		for(int ind=0; ind<maximal_ind_list.size(); ind++)
		{
			int max_curve_ind = maximal_ind_list[ind];
			double max_curve_rate_abs = maximal_rate_list[ind];
			double rate = std::min(max_curve_rate_abs, config_.curve_rate_max);
			double attenuation_rate = 1.0 + (config_.attenuation_rate_min - 1.0) * (rate - config_.curve_rate_min) / (config_.curve_rate_max - config_.curve_rate_min);//curve_rate_minからcurve_rate_maxを1からattenuation_rate_minに変更
			double vel = std::max(lane.waypoints[max_curve_ind].twist.twist.linear.x * attenuation_rate, config_.velocity_min / 3.6);
			double deceleration = config_.deceleration_max * (1-attenuation_rate);
			//std::cout << attenuation_rate << "," << lane.waypoints[max_curve_ind].twist.twist.linear.x * attenuation_rate << "," << deceleration << std::endl;

			int ahead = 1;
			for(int i=max_curve_ind-1; i>=0; i--)
			{
				if(fabs(copy_lane.waypoints[i].waypoint_param.curve_rate) > config_.ahead_rate)
					ahead++;
				else break;
			}
			int behind = 1;
			for(int i=max_curve_ind+1; i<lane.waypoints.size(); i++)
			{
				if(fabs(copy_lane.waypoints[i].waypoint_param.curve_rate) > config_.ahead_rate)
					behind++;
				else break;
			}

			l = apply_acceleration(l, deceleration, max_curve_ind, behind, vel);
			std::reverse(l.waypoints.begin(), l.waypoints.end());
			int reverse_stop_index = l.waypoints.size() - max_curve_ind - 1;
			l = apply_acceleration(l, deceleration, reverse_stop_index, ahead, vel);
			std::reverse(l.waypoints.begin(), l.waypoints.end());
		}
		pub_local_waypoints_.publish(l);*/
	}

	void callbackLocalWaypoints(const autoware_msgs::Lane &msg)
	{
		curveCheck(msg);
	}
public:
	LaneAppropriateChecker(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
	{
		pub_local_waypoints_ = nh_.advertise<autoware_msgs::Lane>("/curve_appropriate_waypoints", 1);
		//pub_local_waypoints_ = nh_.advertise<autoware_msgs::Lane>("/final_waypoints", 1);
		pub_tmp_ = nh_.advertise<std_msgs::String>("/lane_app_tmp", 1);
		sub_config_ = nh_.subscribe("/config/lane_appropriate_checker", 1, &LaneAppropriateChecker::callbackConfig, this);
		sub_local_waypoints_ = nh_.subscribe("/safety_waypoints", 1, &LaneAppropriateChecker::callbackLocalWaypoints, this);
	}
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "lane_appropriate_checker");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	LaneAppropriateChecker lac(nh, private_nh);
	ros::spin();
	return 0;
}