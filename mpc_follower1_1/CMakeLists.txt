cmake_minimum_required(VERSION 2.8.3)
project(mpc_follower1_1)


find_package(autoware_build_flags REQUIRED)

find_package(autoware_msgs REQUIRED)

find_package(autoware_config_msgs REQUIRED)

find_package(tablet_socket_msgs REQUIRED)
find_package(catkin REQUIRED COMPONENTS
        roscpp
        std_msgs
        tf
        geometry_msgs
        pcl_ros
        pcl_conversions
        sensor_msgs
        autoware_msgs
        autoware_config_msgs
        tablet_socket_msgs
        autoware_health_checker
        amathutils_lib
        rostest
        rosunit
        )

################################################
## Declare ROS messages, services and actions ##
################################################

###################################
## catkin specific configuration ##
###################################
catkin_package(
        INCLUDE_DIRS include
        LIBRARIES libwaypoint_follower
        CATKIN_DEPENDS roscpp
        std_msgs
        tf
        geometry_msgs
        autoware_msgs
        autoware_config_msgs
        pcl_ros
        pcl_conversions
        sensor_msgs
        tablet_socket_msgs
        autoware_health_checker
        amathutils_lib
)

###########
## Build ##
###########

SET(CMAKE_CXX_FLAGS "-O2 -g -Wall ${CMAKE_CXX_FLAGS}")

include_directories(
        include
        include/twist_gate
        ${autoware_config_msgs_INCLUDE_DIRS}
        ${autoware_msgs_INCLUDE_DIRS}
        ${tablet_socket_msgs_INCLUDE_DIRS}
        ${catkin_INCLUDE_DIRS}
)

add_library(libwaypoint_follower1_1 lib/libwaypoint_follower.cpp)
add_dependencies(libwaypoint_follower1_1
        ${catkin_EXPORTED_TARGETS}
        )
target_link_libraries(libwaypoint_follower1_1 ${catkin_LIBRARIES})

set(MPC_FOLLOWER_SRC
        mpc_follower/mpc_utils.cpp
        mpc_follower/mpc_trajectory.cpp
        mpc_follower/lowpass_filter.cpp
        mpc_follower/vehicle_model/vehicle_model_interface.cpp
        mpc_follower/vehicle_model/vehicle_model_bicycle_kinematics.cpp
        mpc_follower/vehicle_model/vehicle_model_bicycle_dynamics.cpp
        mpc_follower/vehicle_model/vehicle_model_bicycle_kinematics_no_delay.cpp
        mpc_follower/qp_solver/qp_solver_unconstr.cpp
        mpc_follower/qp_solver/qp_solver_unconstr_fast.cpp
)

add_executable(mpc_follower1_1 mpc_follower/mpc_follower_node.cpp mpc_follower/mpc_follower_core.cpp ${MPC_FOLLOWER_SRC})
add_dependencies(mpc_follower1_1 ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(mpc_follower1_1 ${catkin_LIBRARIES})

add_executable(mpc_waypoints_converter1_1 mpc_follower/mpc_waypoints_converter.cpp)
add_dependencies(mpc_waypoints_converter1_1 ${catkin_EXPORTED_TARGETS})
target_link_libraries(mpc_waypoints_converter1_1 ${catkin_LIBRARIES})

## Install executables and/or libraries
install(TARGETS libwaypoint_follower1_1 mpc_follower1_1 mpc_waypoints_converter1_1
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})

## Install project namespaced headers
install(DIRECTORY include/${PROJECT_NAME}/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION})

install(DIRECTORY launch/
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
        PATTERN ".svn" EXCLUDE)
